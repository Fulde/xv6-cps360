#include "types.h"
#include "stat.h"
#include "user.h"

int
main(int argc, char *argv[])
{
  printf(1, "Daniel Fulde (dfuld044)\n");

  // this pid
  printf(1, "My pid: %d\n", getpid());

  // parent's pid
  printf(1, "Parent's pid: %d\n", getppid());

  exit();
}
	
